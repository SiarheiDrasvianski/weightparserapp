# README #

Educational project

Daily weight records using Parse backend.

### Build guide ###

* Run 
```
#!unix

pod install
```
* Use .xcworkspace file

* Feel free to report issues

### Assumptions & Limitations ###

* Weight is entered at least once a day
* Correct values are entered
* Plot is as simple as possible