//
//  WPWeightTableViewController.h
//  WeightParser
//
//  Created by Siarhei Drasvianski on 23.09.15.
//  Copyright © 2015 Siarhei Drasvianski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import "WPConstants.h"
#import "WPEnterWeightViewController.h"
#import "WPWeightPlotViewController.h"

@interface WPWeightTableViewController : PFQueryTableViewController

@end
