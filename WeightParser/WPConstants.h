//
//  WPConstants.h
//  WeightParser
//
//  Created by Siarhei Drasvianski on 23.09.15.
//  Copyright © 2015 Siarhei Drasvianski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

typedef void (^CallbackBlock)(id object);
typedef void (^SuccessBlock)();
typedef void (^FailureBlock)(NSError *error);
//typedef void (^callbackBlovk)(BOOL succeeded, NSError * _Nullable error);


typedef enum {
    WPRouteSignIn,
    WPRouteSignUp,
    WPRouteWorkflow
}WPRoute;

extern NSString * const WPWeightRecord;
extern NSString * const WPWeight;
extern NSString * const WPUser;
extern NSString * const WPUpdatedAt;
extern NSString * const WPObjectID;

@interface WPConstants : NSObject

@end
