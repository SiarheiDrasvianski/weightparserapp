//
//  AppDelegate.h
//  WeightParser
//
//  Created by Siarhei Drasvianski on 21.09.15.
//  Copyright © 2015 Siarhei Drasvianski. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Parse/Parse.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

