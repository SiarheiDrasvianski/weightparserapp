//
//  ViewController.m
//  WeightParser
//
//  Created by Siarhei Drasvianski on 21.09.15.
//  Copyright © 2015 Siarhei Drasvianski. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

#pragma mark - View life cycle

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self routeViewControllers];
}

#pragma mark - Internal

-(void)routeViewControllers
{
    PFUser *currentUser = [PFUser currentUser];
    if (currentUser) {
        [[WPUserHelper sharedHelper] setRoute:WPRouteWorkflow];
    } 
    
    switch ([[WPUserHelper sharedHelper] route]) {
        case WPRouteSignIn:{
            
            WPSignInViewController *controller = [WPSignInViewController new];
            [self presentViewController:controller animated:NO completion:nil];
            
        }
            break;
            
        case WPRouteSignUp:{
            
            WPSignUpViewController *controller = [WPSignUpViewController new];
            [self presentViewController:controller animated:NO completion:nil];
            
        }
            break;
            
        case WPRouteWorkflow:{
            
            UINavigationController *controller = [[UINavigationController alloc] initWithRootViewController:[WPEnterWeightViewController new]];
            [self presentViewController:controller animated:NO completion:nil];
            
        }
            break;
    }
  
}


@end
