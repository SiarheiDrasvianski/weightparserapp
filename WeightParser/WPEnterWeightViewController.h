//
//  WPEnterWeightViewController.h
//  WeightParser
//
//  Created by Siarhei Drasvianski on 23.09.15.
//  Copyright © 2015 Siarhei Drasvianski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "WPUserHelper.h"
#import "WPParseHelper.h"
#import "WPWeightTableViewController.h"

@interface WPEnterWeightViewController : UIViewController

@property (strong, nonatomic) NSString *currentRecordID;
@property (assign, nonatomic) BOOL historyEditor;
@property (strong, nonatomic) NSNumber *weightToDisplay;

@end
