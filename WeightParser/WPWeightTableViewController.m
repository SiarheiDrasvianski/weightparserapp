//
//  WPWeightTableViewController.m
//  WeightParser
//
//  Created by Siarhei Drasvianski on 23.09.15.
//  Copyright © 2015 Siarhei Drasvianski. All rights reserved.
//

#import "WPWeightTableViewController.h"

@implementation WPWeightTableViewController


#pragma mark - View life cycle

- (instancetype)initWithStyle:(UITableViewStyle)style {
    self = [super initWithStyle:style];
    if (self) {
        self.parseClassName = WPWeightRecord;
        self.pullToRefreshEnabled = YES;
        self.paginationEnabled = NO;
        self.objectsPerPage = 25;
        
        UIBarButtonItem *nextButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Plot" style:UIBarButtonItemStylePlain target:self action:@selector(didTapPlot)];
        self.navigationItem.rightBarButtonItem = nextButtonItem;
        
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self.tableView reloadData];
}


#pragma mark - Delegate/Data source

- (PFQuery *)queryForTable {
    PFQuery *query = [PFQuery queryWithClassName:self.parseClassName];
    [query whereKey:WPUser equalTo:[PFUser currentUser]];

    [query orderByDescending:WPUpdatedAt];
    
    return query;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
                        object:(PFObject *)object {
    static NSString *cellIdentifier = @"cell";
    
    PFTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[PFTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:cellIdentifier];
    }
    

    cell.textLabel.text = [object[WPWeight] stringValue];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Updated: %@",  object[WPUpdatedAt]];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    
    
    WPEnterWeightViewController *controller = [WPEnterWeightViewController new];
    
    controller.historyEditor = YES;
    controller.weightToDisplay = [[self objectAtIndexPath:indexPath] objectForKey:WPWeight];
    controller.currentRecordID = [[self objectAtIndexPath:indexPath] objectId];
    
    [self.navigationController pushViewController:controller animated:YES];
}


#pragma mark - Actions

-(void)didTapPlot
{
    WPWeightPlotViewController *controller = [WPWeightPlotViewController new];
    
    controller.records = self.objects;
    [self.navigationController pushViewController:controller animated:YES];
    
}


@end
