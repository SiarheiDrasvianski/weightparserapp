//
//  WPUserHelper.h
//  WeightParser
//
//  Created by Siarhei Drasvianski on 23.09.15.
//  Copyright © 2015 Siarhei Drasvianski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "WPConstants.h"

@interface WPUserHelper : NSObject

@property (strong, nonatomic) PFUser *user;
@property (assign, nonatomic) WPRoute route;

+ (instancetype)sharedHelper;


@end
