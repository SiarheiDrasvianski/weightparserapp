//
//  WPParseHelper.h
//  WeightParser
//
//  Created by Siarhei Drasvianski on 23.09.15.
//  Copyright © 2015 Siarhei Drasvianski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WPConstants.h"

#import <Parse/Parse.h>
#import "WPUserHelper.h"

@interface WPParseHelper : NSObject

+ (void)signUpUserWithUsername:(NSString *)username password:(NSString *)password successBlock:(SuccessBlock)successBlock failureBlock:(FailureBlock)failureBlock;
+ (void)signInUserWithUsername:(NSString *)username password:(NSString *)password successBlock:(SuccessBlock)successBlock failureBlock:(FailureBlock)failureBlock;
+ (void)saveWeight:(NSNumber *)weight withCallbackBlock:(CallbackBlock)callbackBlock failureBlock:(FailureBlock)failureBlock;
+ (void)getTodayWeightWithCallbackBlock:(CallbackBlock)callbackBlock failureBlock:(FailureBlock)failureBlock;
+ (void)getTodayWeightCountWithCallbackBlock:(CallbackBlock)callbackBlock failureBlock:(FailureBlock)failureBlock;
+ (void)updateWeightRecordOfID:(NSString *)recordID withWeight:(NSNumber *)weight callbackBlock:(CallbackBlock)callbackBlock failureBlock:(FailureBlock)failureBlock;

@end
