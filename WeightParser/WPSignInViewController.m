//
//  WPSignInViewController.m
//  WeightParser
//
//  Created by Siarhei Drasvianski on 23.09.15.
//  Copyright © 2015 Siarhei Drasvianski. All rights reserved.
//

#import "WPSignInViewController.h"

@interface WPSignInViewController ()


@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIButton *signinButton;
@property (weak, nonatomic) IBOutlet UIButton *switchScreenButton;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;

@end

@implementation WPSignInViewController


#pragma mark - View life cycle
#pragma mark - Internal
#pragma mark - Actions

-(IBAction)signIn:(id)sender
{
    [WPParseHelper signInUserWithUsername:self.usernameField.text
                                 password:self.usernameField.text
                             successBlock:^{
                                 
                                 self.errorLabel.hidden = YES;
                                 [[WPUserHelper sharedHelper] setRoute:WPRouteWorkflow];
                                 [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
                                 
                             } failureBlock:^(NSError *error) {
                                 
                                 NSString *errorString = [error userInfo][@"error"];
                                 self.errorLabel.text = errorString;
                                 self.errorLabel.hidden = NO;
                                 
    }];
}

-(IBAction)switchScreen:(id)sender
{
    [[WPUserHelper sharedHelper] setRoute:WPRouteSignUp];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Delegate
//client-side fields validation omitted
//return key handling omitted


@end
