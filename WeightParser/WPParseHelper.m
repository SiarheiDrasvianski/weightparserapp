//
//  WPParseHelper.m
//  WeightParser
//
//  Created by Siarhei Drasvianski on 23.09.15.
//  Copyright © 2015 Siarhei Drasvianski. All rights reserved.
//

#import "WPParseHelper.h"

@implementation WPParseHelper

+ (void)signUpUserWithUsername:(NSString *)username password:(NSString *)password successBlock:(SuccessBlock)successBlock failureBlock:(FailureBlock)failureBlock
{
    PFUser *user = [PFUser user];
    user.username = username;
    user.password = password;

    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            successBlock();
        } else {
            failureBlock(error);
        }
    }];
}

+ (void)signInUserWithUsername:(NSString *)username password:(NSString *)password successBlock:(SuccessBlock)successBlock failureBlock:(FailureBlock)failureBlock
{
    [PFUser logInWithUsernameInBackground:username
                                 password:password
                                    block:^(PFUser *user, NSError *error) {
                                        if (user) {
//                                            [[WPUserHelper sharedHelper] setUser:user];
                                            successBlock();
                                        } else {
                                            failureBlock(error);
                                        }
                                    }];
}

+ (void)saveWeight:(NSNumber *)weight withCallbackBlock:(CallbackBlock)callbackBlock failureBlock:(FailureBlock)failureBlock
{
    PFObject *weightRecord = [PFObject objectWithClassName:WPWeightRecord];
    weightRecord[WPWeight] = weight;
    weightRecord[WPUser] = [PFUser currentUser];
    weightRecord[WPUpdatedAt] = [NSDate date];

    [weightRecord saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
        if (!error) {
            callbackBlock(weightRecord[WPObjectID]);
        } else {
            failureBlock(error);
        }
    }];
}

+ (void)getTodayWeightCountWithCallbackBlock:(CallbackBlock)callbackBlock failureBlock:(FailureBlock)failureBlock
{
    PFQuery *query = [PFQuery queryWithClassName:WPWeightRecord];
    
    
    NSDate *today = [NSDate date];
    NSDate *todayStart = [self dateWithDate:today hour:0 minute:0 second:0];
    NSDate *todayEnd = [self dateWithDate:today hour:23 minute:59 second:59];
    
    [query whereKey:WPUpdatedAt greaterThanOrEqualTo:todayStart];
    [query whereKey:WPUpdatedAt lessThanOrEqualTo:todayEnd];
    [query whereKey:WPUser equalTo:[PFUser currentUser]];
    
    [query countObjectsInBackgroundWithBlock:^(int count, NSError *error) {
        if (!error) {
            callbackBlock(@(count));
        } else {
            failureBlock(error);
        }
    }];

    
    
}

+ (void)getTodayWeightWithCallbackBlock:(CallbackBlock)callbackBlock failureBlock:(FailureBlock)failureBlock
{
    PFQuery *query = [PFQuery queryWithClassName:WPWeightRecord];
    
    
    NSDate *today = [NSDate date];
    NSDate *todayStart = [self dateWithDate:today hour:0 minute:0 second:0];
    NSDate *todayEnd = [self dateWithDate:today hour:23 minute:59 second:59];
    
    [query whereKey:WPUpdatedAt greaterThanOrEqualTo:todayStart];
    [query whereKey:WPUpdatedAt lessThanOrEqualTo:todayEnd];
    [query whereKey:WPUser equalTo:[PFUser currentUser]];

    [query addDescendingOrder:WPUpdatedAt];

    [query getFirstObjectInBackgroundWithBlock:^(PFObject * _Nullable object, NSError * _Nullable error) {
        if (!error) {
            callbackBlock(object);
        } else {
            failureBlock(error);
        }
    }];
    

}

+ (void)updateWeightRecordOfID:(NSString *)recordID withWeight:(NSNumber *)weight callbackBlock:(CallbackBlock)callbackBlock failureBlock:(FailureBlock)failureBlock
{
    PFQuery *query = [PFQuery queryWithClassName:WPWeightRecord];

    [query getObjectInBackgroundWithId:recordID block:^(PFObject *object, NSError *error) {
        
        object[WPWeight] = weight;

        [object saveInBackgroundWithBlock:^(BOOL succeeded, NSError * _Nullable error) {
            if (!error) {
                callbackBlock(object);
            } else {
                failureBlock(error);
            }
        }];
    }];
}

+ (NSDate *) dateWithDate:(NSDate *)date
                    hour:(NSInteger)hour
                  minute:(NSInteger)minute
                  second:(NSInteger)second
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    calendar.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    
    NSDateComponents *components = [calendar components: NSCalendarUnitYear | NSCalendarUnitMonth| NSCalendarUnitDay fromDate:date];
    [components setHour:hour];
    [components setMinute:minute];
    [components setSecond:second];
    
    NSDate *newDate = [calendar dateFromComponents:components];
    return newDate;
}


@end
