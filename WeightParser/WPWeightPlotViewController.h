//
//  WPWeightPlotViewController.h
//  WeightParser
//
//  Created by Siarhei Drasvianski on 24.09.15.
//  Copyright © 2015 Siarhei Drasvianski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "WPConstants.h"

@interface WPWeightPlotViewController : UIViewController

@property (strong, nonatomic) NSArray *records;

@end
