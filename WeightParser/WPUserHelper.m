//
//  WPUserHelper.m
//  WeightParser
//
//  Created by Siarhei Drasvianski on 23.09.15.
//  Copyright © 2015 Siarhei Drasvianski. All rights reserved.
//

#import "WPUserHelper.h"

@implementation WPUserHelper

#pragma mark - Initialization

+ (instancetype)sharedHelper;
{
    static WPUserHelper *sharedHelper = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedHelper = [[self alloc] init];
    });
    return sharedHelper;
}

@end
