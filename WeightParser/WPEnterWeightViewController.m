//
//  WPEnterWeightViewController.m
//  WeightParser
//
//  Created by Siarhei Drasvianski on 23.09.15.
//  Copyright © 2015 Siarhei Drasvianski. All rights reserved.
//

#import "WPEnterWeightViewController.h"

@interface WPEnterWeightViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *weightField;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;
@property (weak, nonatomic) IBOutlet UILabel *successLabel;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *historyButton;


@end

@implementation WPEnterWeightViewController

#pragma mark - View life cycle

-(void)viewWillAppear:(BOOL)animated
{

    self.title = @"Enter weight";
    
    if (self.historyEditor) {
        
        [self fillAnyWeight];
        
    } else{
        
        [self fillTodayWeight];
        
    }
    
}


#pragma mark - Internal

-(void)hideSuccessLabel
{
    self.successLabel.hidden = YES;
}

-(void)handleError:(NSError *)error
{
    self.successLabel.hidden = YES;
    NSString *errorString = [error userInfo][@"error"];
    self.errorLabel.text = errorString;
    self.errorLabel.hidden = NO;
}

-(void)handleSuccess
{
    self.saveButton.enabled = YES;
    self.errorLabel.hidden = YES;
    self.successLabel.hidden = NO;
    [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(hideSuccessLabel) userInfo:nil repeats:NO];
}

-(void)fillAnyWeight
{
    self.historyButton.hidden = YES;
    self.weightField.text = self.weightToDisplay.stringValue;
}

-(void)fillTodayWeight
{
    self.weightField.enabled = NO;
    self.saveButton.enabled = NO;

    
    [WPParseHelper getTodayWeightCountWithCallbackBlock:^(id object) { //if any records exist, load first one
        
        if ([object integerValue] > 0) {
            
            [WPParseHelper getTodayWeightWithCallbackBlock:^(PFObject *object) {
                
                self.weightField.text = [object[WPWeight] stringValue];
                self.currentRecordID = object.objectId;
                
                self.weightField.enabled = YES;
                self.saveButton.enabled = YES;
                
            } failureBlock:^(NSError *error) {
                [self handleError:error];
            }];
            
        } else{
            
            self.weightField.enabled = YES;
            self.saveButton.enabled = YES;
            self.currentRecordID = nil;
            
        }
    } failureBlock:^(NSError *error) {
        [self handleError:error];
    }];
}


#pragma mark - Actions

-(IBAction)saveWeight:(id)sender
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *weight = [formatter numberFromString:self.weightField.text];
    
    if (weight) {
        
        self.saveButton.enabled = NO;
        [self.view endEditing:YES];
        
        if (self.currentRecordID) {
            
            [WPParseHelper updateWeightRecordOfID:self.currentRecordID withWeight:weight callbackBlock:^(id object) {
                
                [self handleSuccess];
                
            } failureBlock:^(NSError *error) {
                
                self.saveButton.enabled = YES;
                [self handleError:error];
                
            }];
            
            
        } else{
            
            [WPParseHelper saveWeight:weight withCallbackBlock:^(id object) {
                
                [self handleSuccess];
                self.currentRecordID = object;

            } failureBlock:^(NSError *error) {
                
                self.saveButton.enabled = YES;
                [self handleError:error];
                
            }];
            
        }
    }
}

-(IBAction)tapHistory:(id)sender
{
    WPWeightTableViewController *controller = [WPWeightTableViewController new];
    [self.navigationController pushViewController:controller animated:YES];
    
}


#pragma mark - UITextFieldDelegate

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    return YES;
}

@end
