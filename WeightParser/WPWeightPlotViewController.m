//
//  WPWeightPlotViewController.m
//  WeightParser
//
//  Created by Siarhei Drasvianski on 24.09.15.
//  Copyright © 2015 Siarhei Drasvianski. All rights reserved.
//

#import "WPWeightPlotViewController.h"

@interface WPWeightPlotViewController ()

@property (strong, nonatomic) NSNumber *maxWeight;
@property (strong, nonatomic) NSNumber *minWeight;

@end

@implementation WPWeightPlotViewController


#pragma mark - View life cycle

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self placeSubviews];
}


#pragma  mark - Internal

-(void)placeSubviews
{
    self.maxWeight = @0.0;
    self.minWeight = [NSNumber numberWithFloat:MAXFLOAT];
    
    for (PFObject *record in self.records) {
        if ([record[WPWeight] floatValue] > self.maxWeight.floatValue) {
            self.maxWeight = [NSNumber numberWithFloat:[record[WPWeight] floatValue]];
        }
        
        if ([record[WPWeight] floatValue] < self.minWeight.floatValue) {
            self.minWeight = [NSNumber numberWithFloat:[record[WPWeight] floatValue]];
        }
    }
    
    CGFloat border = 8.0;
    CGFloat columnWidth = 40.0;
    CGFloat bottomAddedHeight = 5.0;
    
    CGRect scrollFrame = CGRectZero;
    scrollFrame.origin.x = 0.0;
    scrollFrame.origin.y = 66.0 + border;
    scrollFrame.size.width = scrollFrame.size.height = self.view.frame.size.width;
    
    CGFloat zoom = (scrollFrame.size.height - bottomAddedHeight) / (self.maxWeight.floatValue - self.minWeight.floatValue);
    CGFloat plotHeight = scrollFrame.size.height;
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:scrollFrame];
    
    CGRect columnFrame = CGRectZero;
    columnFrame.size.width = columnWidth;
    
    for (PFObject *record in self.records) {
        
        if ([self isThisMonth:record[WPUpdatedAt]]) {
        
            columnFrame.size.height = ([record[WPWeight] floatValue] - self.minWeight.floatValue ) * zoom + bottomAddedHeight;
            columnFrame.origin.y = plotHeight - columnFrame.size.height;
            
            UIView *columnView = [[UIView alloc] initWithFrame:columnFrame];
            columnView.backgroundColor = [UIColor orangeColor];
            
            [scrollView addSubview:columnView];
            
            scrollView.contentSize = CGSizeMake(CGRectGetMaxX(columnFrame), plotHeight);
            
            columnFrame.origin.x += columnWidth + 1.0;
        }
    }
    
    [self.view addSubview:scrollView];
}

-(NSDate *)monthsStart
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components: NSCalendarUnitYear | NSCalendarUnitMonth fromDate:[NSDate date]];
    [components setDay:0];
    [components setHour:0];
    [components setMinute:0];
    [components setSecond:0];
    NSDate *newDate = [calendar dateFromComponents:components];
    return newDate;
}

-(BOOL)isThisMonth:(NSDate *)date
{
    if ([date compare:[self monthsStart]] == NSOrderedDescending) {
        return YES;
    } else {
        return NO;
    }
}
@end
