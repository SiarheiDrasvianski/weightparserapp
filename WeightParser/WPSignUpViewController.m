//
//  WPSignUpViewController.m
//  WeightParser
//
//  Created by Siarhei Drasvianski on 23.09.15.
//  Copyright © 2015 Siarhei Drasvianski. All rights reserved.
//

#import "WPSignUpViewController.h"

@interface WPSignUpViewController ()


@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIButton *signupButton;
@property (weak, nonatomic) IBOutlet UIButton *switchScreenButton;
@property (weak, nonatomic) IBOutlet UILabel *errorLabel;


@end

@implementation WPSignUpViewController

#pragma mark - View life cycle
#pragma mark - Internal
#pragma mark - Actions

-(IBAction)signUp:(id)sender
{
    [WPParseHelper signUpUserWithUsername:self.usernameField.text password:self.passwordField.text successBlock:^{
        
        self.errorLabel.hidden = YES;
        [[WPUserHelper sharedHelper] setRoute:WPRouteSignIn];
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
        
    } failureBlock:^(NSError *error) {
        
        NSString *errorString = [error userInfo][@"error"];
        self.errorLabel.text = errorString;
        self.errorLabel.hidden = NO;
        
    }];
}

-(IBAction)switchScreen:(id)sender
{
    [[WPUserHelper sharedHelper] setRoute:WPRouteSignIn];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Delegate
//client-side fields validation omitted
//return key handling omitted

@end
