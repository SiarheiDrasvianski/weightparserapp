//
//  WPConstants.m
//  WeightParser
//
//  Created by Siarhei Drasvianski on 23.09.15.
//  Copyright © 2015 Siarhei Drasvianski. All rights reserved.
//

#import "WPConstants.h"

NSString * const WPWeightRecord = @"weightRecord";
NSString * const WPWeight       = @"weight";
NSString * const WPUser         = @"user";
NSString * const WPUpdatedAt    = @"weightUpdatedAt";
NSString * const WPObjectID     = @"objectId";

@implementation WPConstants

@end
