//
//  ViewController.h
//  WeightParser
//
//  Created by Siarhei Drasvianski on 21.09.15.
//  Copyright © 2015 Siarhei Drasvianski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "WPSignUpViewController.h"
#import "WPSignInViewController.h"
#import "WPEnterWeightViewController.h"
#import "WPUserHelper.h"

@interface ViewController : UIViewController


@end

